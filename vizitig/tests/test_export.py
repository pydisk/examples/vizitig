from pathlib import Path

from pytest import raises

from vizitig.errors import EmptyExportError
from vizitig.export import export_graph


def test_export_graph_bcalm_in_normal_conditions(
    tmp_path: Path, viz_dir, test_graph_name
):
    test_file = tmp_path / "file"
    export_graph(test_graph_name, [165114, 248683, 459264], "bcalm", test_file)
    assert test_file.exists()
    test_file.unlink()


def test_export_graph_bcalm_subgraph(tmp_path: Path, viz_dir, test_graph_name):
    test_file = tmp_path / "file"
    sample_nodes = [165114, 248683, 459264]

    export_graph(test_graph_name, sample_nodes, "bcalm", test_file)
    assert test_file.exists()
    test_file.unlink()


def test_export_graph_bcalm_crashes_with_bad_nodes_id(
    tmp_path: Path, viz_dir, test_graph_name
):
    test_file = tmp_path / "file"
    with raises(EmptyExportError):
        export_graph(test_graph_name, list([-1]), "bcalm", test_file)
    assert not test_file.exists()
