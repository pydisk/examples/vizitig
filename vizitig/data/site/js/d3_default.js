export let default_prop = {
    "charge_radius":350,
    "charge_strength": -250,
    "link_strength": 0.6,
    "alpha": 2.2,
    "alpha_decay": 0.008,
};
