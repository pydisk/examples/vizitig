# TODO

- long running task management: right now if something kill the process the subprocess continue, there is no feedback in case of interrompted process.
- Broken graphs (typically generate_graph broken midway) can simply cause crashed, we want to improve reliability of that
- Log management: so far log are not archive, we may want some tools for that
- Adding description of graphs and maybe add per node annotation
- Better index, with compiled code
