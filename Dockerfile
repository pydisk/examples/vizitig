FROM python:3.12

COPY ./ /app/
WORKDIR /app
RUN pip index versions vizibridge
RUN pip --no-cache-dir install .[all]
