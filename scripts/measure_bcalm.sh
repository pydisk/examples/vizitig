#!/bin/bash

avg_degree=true;
min_size_unitig=false;
avg_size_unitig=false;
while getopts "amdh" opt
do
    case $opt in
        d) avg_degree=true
           min_size_unitig=false
           avg_size_unitig=false
           ;;
        m) avg_degree=false
           min_size_unitig=true
           avg_size_unitig=false
           ;;
        a) avg_degree=false
           min_size_unitig=false
           avg_size_unitig=true
           ;;
        h) echo "Usage: $0 [-amdh] path/to/bcalm.fa"
            echo "at most one option to set"
            echo
            echo " -d  measure the avg degree of the graph [default]"
            echo " -m  measure the min size of a unitig node"
            echo " -a  measure the avg size of a unitig node"
            echo " -h  show this help and exit. "
            exit 0;;
    esac
done
shift $((OPTIND-1))
if [ $# -eq 0 ]; then
    echo "Error: path to bcalm.fa is required"
    echo "Usage: $0 [-amdh] path/to/bcalm.fa"
    exit 1
fi
bcalm_path=$1

# Conditional branches based on the options set
if [ "$avg_degree" = true ]; then
    a=$(grep -o "L:" $bcalm_path  | wc -l)
    b=$(grep -c ">" $bcalm_path)
    bc <<< "scale=2; $a/$b"
elif [ "$min_size_unitig" = true ]; then
    echo $(grep -v '^>' "$bcalm_path" | grep -v '^$' | while read -r line; do echo ${#line}; done | sort -n | head -n 1)
elif [ "$avg_size_unitig" = true ]; then
    a=$(grep -v '^>' "$bcalm_path" | grep -v '^$' | while read -r line; do echo ${#line}; done | sort -n | paste -s -d+ | bc)
    b=$(grep -c ">" $bcalm_path)
    bc <<< "scale=2; $a/$b"
fi
