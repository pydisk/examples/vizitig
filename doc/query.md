# The query language spec 

Here a description of the operation supported by the DSL:

## Atomic operations



* Selection: 
    * represents the selected nodes in the current view of the graph
    * alternative: selection, Selection(), selection()
    * Client side only

* Partial: 
    * represent the nodes connected to node loaded in the current view of the graph
    * alternative: partial, partial(), Partial()
    * Client side only, 

* Degree(some_number):
    * return all nodes with the exact degree.  
    * Example: Degree(1) return all tigs nodes.
    * alternative: degree(1), degree(2)
    * Client side only, 

* Loop:
    * Returns nodes that are looping on themself
    * Client side only

* All: All nodes in the graph. 
    * Alternative All(), all, all()
    * By default, only get 1000 nodes.

* NodeId(some_number): 
    * return the node with the given NodeId. 
    * Example: NodeId(1234), nodeid(1234)
    * Complexity: O(1)
    * NodeId allows also enumeration in it: NodeId(12, 22, 234) as a shorthand for NodeId(12) OR NodeId(22) OR NodeId(234). The version with enumeration is better optimized.

* Color(some_color): 
    * return all nodes tag with the color.
    * Exampe: Color(sample1) 
    * Attention: the color name are case sensitive
    * Complexity: O(p) where p is the size of the outputed set. 
    * By default, only get 1000 nodes.
    * Optionally, Color can be queried with their abundance Color(X, A<12), Color(X, A<=12), Color(X, A=12), Color(X, A>12), Color(X, A>=12)


* some_meta_data(*arg, **kwargs): 
    * return all nodes with some_meta_data and some extra condition. 
    * Example: transcript(AVX124, start>12).
    * Attention: the name are case sensitives
    * Complexity: O(p) where p is the size of the outputed set. 
    * By default, only get 1000 nodes.

* Kmer(dna_seq): 
    * return the node that contains the kmer.  
    * dna_seq should of size k. 
    * Example: Kmer(ACGTCGT)
    * Complexity: O(1) if an index is built OR if kmer are ingested. Otherwise O(n) with n the size of the graph with a linear scan of all the unitig sequence.

* Seq(dna_seq): 
    * return all nodes that contains the sequence. 
    * Example: Seq(ACGT).
    * By default, only get 1000 nodes
    * The complexity is complicated. If the sequence is shorter than k, it is O(n) with n the size of the Graph with a linear scan and pattern matching on the sequence.
    If the sequence is longer than k, then we try to find the node that contains on of the kmer of the sequence and we become O(1).

## Combinators


We allow arbitrary boolean combination of atomic operations and parethensis.

* A1 OR A2
* (A1 OR A2) AND A3
* OR(A1, A2, A3, A4, A5)
* NOT A3

## Some examples

* (Color(sample1) and Color(sample2)) AND Not Transcript(XXX)
* Or(NodeID(1), NodeID(3), NodeID(4))

## Formal specification: 

We rely on [Lark]() for generating the parser
from the grammar. The grammar is described formally here
but a (might be up to date) version can be found
in the `vizitig/query/query.py`


```
?formula: lor_infix  
        | lor_prefix
        | land 
        | lnot 
        | par  
        | literal

_LEFTPAR  : /\s*\(\s*/ 
_RIGHTPAR : /\s*\)\s*/
?par      : _LEFTPAR formula _RIGHTPAR 
_SEP      : /\s*,\s*/
lor_infix : formula "or"i (_SPACES formula | par) 
lor_prefix: "or"i _LEFTPAR (formula ( _SEP formula )*)? _RIGHTPAR
land      : formula "and"i (_SPACES formula | par)
lnot      : "not"i (_SPACES formula | par)
_SPACES   : /\s+/
        
?literal: nodeid 
        | kmer 
        | color
        | psdomap
        | meta
        | seq
        | all
        | loop
        | partial
        | selection 
        | degree 
    
selection.1: "selection"i ((_LEFTPAR _RIGHTPAR) |)
partial.1  : "Partial"i ((_LEFTPAR _RIGHTPAR) |)
all.1      : "All"i ((_LEFTPAR _RIGHTPAR) |)
loop.1      : "loop"i ((_LEFTPAR _RIGHTPAR) |)
nodeid.1   : "NodeId"i _LEFTPAR  integer ("," integer)* _RIGHTPAR
color.1    : "Color"i _LEFTPAR  ident ("," abundance |)  _RIGHTPAR
degree.1   : "Degree"i _LEFTPAR integer _RIGHTPAR
meta.0     : ident _LEFTPAR (arg ( _SEP  arg )* | ) _RIGHTPAR
?arg       : ident | attr

abundance  : "A" op integer
kmer.2     : "Kmer"i _LEFTPAR acgt  _RIGHTPAR
attr       : ident op (ident | integer)
seq.1      : "Seq"i _LEFTPAR acgt _RIGHTPAR
psdomap.1  : ("PseudoMapping"i | "PM"i ) _LEFTPAR acgt _RIGHTPAR
?op        : (EQUAL | LT | GT | LTE | GTE)

EQUAL      : "="
LT         : "<"
GT         : ">"
LTE        : "<="
GTE        : ">="

acgt       : /[ACGT]+/i 
integer    : INT
ident      : /[a-zA-Z_][\w\.\-\_]*/i
    
%import common.CNAME
%import common.INT
%import common.WS 
%ignore WS
```


## Client side queries

Some queries are used to act on the graph visualisation tools and are not meant to be used to fetch nodes
in the graph. For instance `Selection` which is a query representing selected nodes or `Degree(2)` which 
allows to select nodes with a certain amount of neighbours.
