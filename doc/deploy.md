# Vizitig as a hosted web application

It is possible to host Vizitig as a web application on your own server and to
access it through internet.  This could be usefull to make use of computing
power and/or disk capacity of a remote server.

`vizitig` backend is based on `FastApi` which can easily be deployed with many
strategies. The [documentation of FastAPI](https://fastapi.tiangolo.com/deployment/)
provides a lot of details about this.


The instance of vizitig running [here](https://networkdisk.inria.fr/vizitig)
for demonstration is on a debian OS with 

* nginx
* unit 

To run a similar configration you can:

1. [install nginx and unit](https://unit.nginx.org/installation/)
2. Follows the documentation of unit to make you web application available.

For instance assuming vizitig root folder is in the folder `/var/`, the  [following document](unit/unit.json)
can be send to unit to spin the application
directly.

```json
{
    "listeners": {
        "*:4242": {
            "pass": "applications/fastapi"
        }
    },

    "applications": {
        "fastapi": {
            "type": "python 3.11",
            "path": "/var/vizitig/",
            "home": "/var/vizitig/venv",
            "module": "vizitig.api",
            "callable": "main_api",
            "environment": {
                "VIZITIG_DIR": "/etc/vizitig/"
            }
        }
    }
}
```

For that, we can simply push the file to unit as follow:

```bash
sudo curl -X PUT --data-binary @unit.json --unix-socket /var/run/control.unit.sock http://localhost/config
```

Then, to make the API accessible, `nginx` can be configured by adding the following directive to simply proxy some route of your web server to the API endpoint:

```
location /vizitig {
    proxy_pass http://127.0.0.1:4242/;
}
```

## Extra remarks

We setup the `VIZITIG_DIR` env variable on the way, which means that all the graph and extra data will
be stored in `/etc/vizitig/`.
