The project relies on `ruff` and `mypy`.

If you install the project with `make install` on linux, it will install all
dependencies in a virtual env (venv).
To activate the venv in bash

```bash
source venv/bin/activate
```

## Before pushing

Run the following commands:

```bash
mypy vizitig
```

This will type check the module. All errors excepts those from external dependencies
must be fixed or documented before pushing.


The project is linted using `ruff`. Please uses the following commands.

```bash
ruff check vizitig
ruff format vizitig
```
