install: 
	python3 -m venv venv
	bash -c "source venv/bin/activate;pip install -e .[dev]"

clean:
	git clean -xf



small_ex:
	bash -c "source venv/bin/activate;python3 -m vizitig build examples/mini_bcalm.fa -n mini_bcalm"
	bash -c "source venv/bin/activate;python3 -m vizitig index build mini_bcalm"
	bash -c "source venv/bin/activate;python3 -m vizitig index build mini_bcalm --small-k 2"
	bash -c "source venv/bin/activate;python3 -m vizitig annotate mini_bcalm -e examples/mini_exons.fa"
	bash -c "source venv/bin/activate;python3 -m vizitig annotate mini_bcalm --transcripts examples/mini_ref.fa -m examples/mini_annot.gtf"
	bash -c "source venv/bin/activate;python3 -m vizitig color -f examples/mini_sample1.fa -d sample -m sample1 mini_bcalm"
	bash -c "source venv/bin/activate;python3 -m vizitig color -f examples/mini_sample2.fa.gz -d sample -m sample2 mini_bcalm"
	bash -c "source venv/bin/activate;python3 -m vizitig color -f examples/abundances.fa -d sample -m sample3 -abundances mini_bcalm"

small_ex_alt:
	bash -c "source venv/bin/activate;python3 -m vizitig build examples/mini_bcalm.fa -n mini_bcalm_alt1"
	bash -c "source venv/bin/activate;python3 -m vizitig index build mini_bcalm_alt1"
	bash -c "source venv/bin/activate;python3 -m vizitig annotate mini_bcalm_alt1 --transcripts examples/mini_ref.fa -m examples/mini_annot.gtf"
	bash -c "source venv/bin/activate;python3 -m vizitig color mini_bcalm_alt1 --folder examples/folder"
	bash -c "source venv/bin/activate;python3 -m vizitig color -f examples/abundances.fa -d sample -m sample3 -abundances mini_bcalm_alt1"
small_ex_alt2:
	bash -c "source venv/bin/activate;python3 -m vizitig build examples/mini_bcalm.fa -n mini_bcalm_alt2"
	bash -c "source venv/bin/activate;python3 -m vizitig index build mini_bcalm_alt2"
	bash -c "source venv/bin/activate;python3 -m vizitig annotate mini_bcalm_alt2 --transcripts examples/mini_ref.fa -m examples/mini_annot.gtf"
	bash -c "source venv/bin/activate;python3 -m vizitig color mini_bcalm_alt2 --csv_file examples/csv.csv"
	bash -c "source venv/bin/activate;python3 -m vizitig color -f examples/abundances.fa -d sample -m sample3 -abundances mini_bcalm_alt2"
